/*   Задача на абстрактные классы:
 * Создать абстрактный класс Pet с полями name, age, hungry(хочет есть) и абстрактный
 * метод voice(голос). Создать классы Snake, Dog, PatrolDog, Cat, Fish и наследники
 * класса Pet. В каждом классе реализовать метод voice.
 *   Задача на интерфейсы:
 * Реализовать два интерфейса PassangersAuto(описать метод перевозки пассажиров) и
 * CargoAuto (описать метод перевозки груза). Написать классы Truck, Sedan, Pickup
 * реализующие эти интерфейсы.

 */

/**
 *
 * @author Виктор
 */
public class main {
    public static void main(String[] args) {
       
    }
    
class Snake extends Pet {
    double length;
    @Override
    void voice() {
        System.out.println("Шшш");
    }
}
class Dog extends Pet {
    @Override
    void voice() {
        System.out.println("Гав");
    }
}
class PatrolDog extends Dog {
    @Override
    void voice() {
        System.out.println("Ррр");
    }
}
class Cat extends Pet {
    @Override
    void voice() {
        System.out.println("Мяу");
    }
}
class Fish {
     void voice() {
    }
}

interface PassangersAuto {
    void transportPassangers();
}
interface CargoAuto {
    void transportCargo();
}
class Truck implements CargoAuto {
    @Override
    public void transportCargo() {
        System.out.println("Перевожу груз");
    }
}
class Sedan implements PassangersAuto {
    @Override
    public void transportPassangers() {
        System.out.println("Перевожу пассажиров");
    }
}
class Pickup implements CargoAuto, PassangersAuto {
    @Override
    public void transportCargo() {
        System.out.println("Перевожу груз");
    }
    @Override
    public void transportPassangers() {
        System.out.println("Перевожу пассажиров");
    }
}

}